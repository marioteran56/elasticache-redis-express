const express = require('express');
const { getUserSession, setUserSession, getAllUserSessions, deleteUserSession } = require('./users');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.get('/userSessions', async (req, res) => {
    const value = await getAllUserSessions();
    res.send(value);
});

app.get('/userSessions/:id', async (req, res) => {
    const id = req.params.id;
    const value = await getUserSession(id);
    res.send(value);
});

app.post('/userSessions/:id', async (req, res) => {
    const id = req.params.id;
    const user = req.body;
    const result = await setUserSession(id, user);
    if (result == 0) {
        res.send('User updated');
    } else {
        res.send('User created');
    }
});

app.put('/userSessions/:id', async (req, res) => {
    const id = req.params.id;
    const user = req.body;
    const result = await setUserSession(id, user);
    if (result == 0) {
        res.send('User updated');
    } else {
        res.send('User created');
    }
});

app.delete('/userSessions/:id', async (req, res) => {
    const id = req.params.id;
    const result = await deleteUserSession(id);
    if (result == 0) {
        res.send('User not found');
    } else {
        res.send('User deleted');
    }
});

app.listen(3000, () => {
    console.log('Server is listening on port 3000');
    }
);