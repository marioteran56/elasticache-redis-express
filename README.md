# ElastiCache Redis Express

Este proyecto es un ejemplo de como usar ElastiCache Redis con Express mediante el uso de [redis](https://www.npmjs.com/package/redis).

## Uso

Antes de ejecutar el proyecto, será neceario crear un **clúster de Redis** en **ElastiCache**. Para ello, se puede seguir la [guía de inicio rápido](https://docs.aws.amazon.com/es_es/AmazonElastiCache/latest/red-ug/GettingStarted.html) de AWS. 

Posteriormente, será necesario realizar un **Port Forwarding** por medio de una instancia de **EC2** para poder acceder al clúster de Redis desde la aplicación.

![Port Forwarding](./port-forwarding.png)

O bien, podemos configurar el **Cliente VPN de AWS** para poder acceder al clúster de Redis desde nuestra computadora, siguiendo la [guía de configuración](https://docs.aws.amazon.com/AmazonElastiCache/latest/mem-ug/accessing-elasticache.html) de AWS.

Asimismo, será necesario crear un nuevo archivo dentro de la carpeta `src` llamado `users.js` con el siguiente contenido:

1. Importar el módulo **redis** y crear un cliente de redis.

```javascript
// Importamos redis
const redis = require('redis');

// Creamos el cliente de redis
const client = redis.createClient({
    host: '127.0.0.1',
    port: 6379
});
```

2. Manejamos los errores que puedan surgir al conectarnos con el clúster de Redis.

```javascript
// Manejamos los errores de conexión
client.on('error', (err) => {
    console.log('Error connecting to Redis', err);
});
```

3. Nos conectamos al clúster de Redis.

```javascript
// Conectamos el cliente
client.connect();
```

4. Creamos una función para crear una sesión de usuario.

```javascript
// Crear un usuario
const setUserSession = async (id, user) => {
    const result = await client.hSet('userSessions', id, JSON.stringify(user));
    return result;
};
```

5. Creamos una función para obtener una sesión de usuario.

```javascript
// Obtener un usuario
const getUserSession = async (id) => {
    const result = await client.hGet('userSessions', id);
    return result;
}
```

6. Creamos una función para obtener todas las sesiones de usuario.

```javascript
// Obtener todos los usuarios
const getAllUserSessions = async () => {
    const result = await client.hGetAll('userSessions');
    return result;
}
```

7. Creamos una función para eliminar una sesión de usuario.

```javascript
// Eliminar un usuario
const deleteUserSession = async (id) => {
    const result = await client.hDel('userSessions', id);
    return result;
}
```

8. Exportamos las funciones.

```javascript
// Exportamos las funciones
module.exports = {
    getUserSession,
    setUserSession,
    getAllUserSessions,
    deleteUserSession
};
```

## Requisitos

- Node.js 16.x o superior
- Npm
    - Express
    - Redis
    - Supervisor

## Ejecución

Una vez que se haya configurado el clúster de Redis y el archivo `users.js`, proceemos a instalar las dependencias.

```bash
npm install
```

Luego ejecutamos el proyecto.

```bash
npm run dev
```